#include "MainFrame.h"
#include "EditorPanel.h"
#include <wx/filedlg.h>
#include <wx/filename.h>
#include <wx/msgdlg.h>


MainFrame::MainFrame(wxWindow* parent)
    : MainFrameBase(parent),
	m_lastNewFile(0)
{
#ifdef _WIN32
	SetIcon(wxIcon(wxT("simpleide")));
#endif
	OpenEditor(true, _GetNewFilename());
	SetStatusText(_("Welcome to Simple IDE!"));
}

MainFrame::~MainFrame()
{
}

void MainFrame::OnNewFile(wxCommandEvent& event)
{
	OpenEditor(true, _GetNewFilename());
}

void MainFrame::OnOpenFile(wxCommandEvent& event)
{
	wxString filename = _GetFilename(_("Open File"), "", wxFD_OPEN);
	if (filename.empty())
		return;

	OpenEditor(false, filename);
}

void MainFrame::OnSaveFile(wxCommandEvent& event)
{
	EditorPanel* editor = GetCurrentEditor();
	if (!editor || !editor->IsModified())
		return;

	if (!editor->HasFilename())
	{
		wxString filename = _GetFilename(_("Save File"), "", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
		if (filename.empty())
			return;
		_SetEditorFilename(editor, filename);
	}

	SaveEditor(editor, editor->GetFilename());
}

void MainFrame::OnSaveAsFile(wxCommandEvent& event)
{
	EditorPanel* editor = GetCurrentEditor();
	if (!editor)
		return;

	wxString filename = _GetFilename(_("Save File"), editor->GetFilename(), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (filename.empty())
		return;

	SaveEditor(editor, filename);
}

void MainFrame::OnCloseFile(wxCommandEvent& event)
{
	CloseEditor(GetCurrentEditor(), true);
}

void MainFrame::OnExit(wxCommandEvent& event)
{
	Close();
}

void MainFrame::OnAbout(wxCommandEvent& event)
{
}

void MainFrame::OnPanelClose(wxAuiNotebookEvent& event)
{
	int page = event.GetSelection();
	if (!CloseEditor(GetEditor(page), false))
		event.Veto();
}

EditorPanel* MainFrame::OpenEditor(bool bNew, const wxString& filename)
{
	EditorPanel* editor = _CreateEditor(bNew, filename);
	if (!bNew)
		editor->OpenFile();
	return editor;
}

bool MainFrame::CloseEditor(EditorPanel* editor, bool bDestroy)
{
	if (editor->IsModified())
	{
		wxMessageDialog dlg(this, _("Do you want to save current file?"), _("File Modified"), wxYES | wxNO);
		if (dlg.ShowModal() != wxID_YES)
			return false;

		if (!editor->HasFilename())
		{
			wxString filename = _GetFilename(_("Save File"), "", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
			if (filename.empty())
				return false;
			_SetEditorFilename(editor, filename);
		}
		SaveEditor(editor, editor->GetFilename());
	}

	if (bDestroy)
		_DestroyEditor(editor);

	return true;
}

bool MainFrame::SaveEditor(EditorPanel* editor, const wxString& filename)
{
	_SetEditorFilename(editor, filename);

	return editor->SaveFile();
}

EditorPanel* MainFrame::_CreateEditor(bool bNew, const wxString& filename)
{
	EditorPanel* editor = new EditorPanel(this);
	m_notebook->AddPage(editor, filename, true);

	if (!bNew)
		_SetEditorFilename(editor, filename);

	return editor;
}

void MainFrame::_DestroyEditor(EditorPanel* panel)
{
	size_t index = m_notebook->GetPageIndex(panel);
	if (index != wxNOT_FOUND)
		m_notebook->RemovePage(index);
	delete panel;
}

EditorPanel* MainFrame::GetCurrentEditor() const
{
	wxWindow* panel = m_notebook->GetCurrentPage();
	if (panel)
		return static_cast<EditorPanel*>(panel);
	return NULL;
}

EditorPanel* MainFrame::GetEditor(size_t index) const
{
	wxWindow* panel = m_notebook->GetPage(index);
	if (panel)
		return static_cast<EditorPanel*>(panel);
	return NULL;
}

void MainFrame::_SetEditorFilename(EditorPanel* editor, const wxString& filename)
{
	if (!editor)
		return;

	editor->SetFilename(filename);

	size_t index = m_notebook->GetPageIndex(editor);
	if (index != wxNOT_FOUND)
	{
		wxFileName fn(filename);
		m_notebook->SetPageText(index, fn.GetFullName());
		m_notebook->SetPageToolTip(index, fn.GetFullPath());
	}
}

wxString MainFrame::_GetFilename(const wxString& title, const wxString& basename, long style)
{
	wxFileName filename(basename);

	wxFileDialog fileDialog(this, title, filename.GetPath(), filename.GetFullName(),
		_("C and C++ files (*.c;*.cc;*.cpp)|*.c;*.cc;*.cpp"), style);

	if (fileDialog.ShowModal() == wxID_CANCEL)
		return wxEmptyString;

	return fileDialog.GetFilename();
}

wxString MainFrame::_GetNewFilename()
{
	return wxString::Format(_("New %d"), ++m_lastNewFile);
}
