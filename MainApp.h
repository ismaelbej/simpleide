#ifndef MAINAPP_H
#define MAINAPP_H

#include <wx/app.h>


class MainApp : public wxApp
{
public:
	MainApp();
	virtual ~MainApp();

	virtual bool OnInit();
};

DECLARE_APP(MainApp);

#endif // MAINAPP_H
