#ifndef MAINFRAME_H
#define MAINFRAME_H

#include "MainFrameBase.h"
#include <vector>


class EditorPanel;


class MainFrame : public MainFrameBase
{
public:
    MainFrame(wxWindow* parent);
    virtual ~MainFrame();

	EditorPanel* OpenEditor(bool bNew, const wxString& filename);
	bool CloseEditor(EditorPanel* editor, bool bDestroy);
	bool SaveEditor(EditorPanel* editor, const wxString& filename);

public:

	EditorPanel* GetCurrentEditor() const;
	EditorPanel* GetEditor(size_t index) const;

protected:
    virtual void OnPanelClose(wxAuiNotebookEvent& event);
    virtual void OnSaveAsFile(wxCommandEvent& event);
    virtual void OnNewFile(wxCommandEvent& event);
    virtual void OnAbout(wxCommandEvent& event);
    virtual void OnCloseFile(wxCommandEvent& event);
    virtual void OnExit(wxCommandEvent& event);
    virtual void OnOpenFile(wxCommandEvent& event);
    virtual void OnSaveFile(wxCommandEvent& event);

private:
	EditorPanel* _CreateEditor(bool bNew, const wxString& filename);
	void _DestroyEditor(EditorPanel* editor);
	void _SetEditorFilename(EditorPanel* editor, const wxString& filename);

	wxString _GetFilename(const wxString& title, const wxString& basename, long style);
	wxString _GetNewFilename();

private:
	int m_lastNewFile;
};

#endif // MAINFRAME_H
