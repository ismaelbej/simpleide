#ifndef EDITORPANEL_H
#define EDITORPANEL_H

#include "EditorPanelBase.h"


class EditorPanel : public EditorPanelBase
{
public:
    EditorPanel(wxWindow* parent);
    virtual ~EditorPanel();
	
	bool IsModified() const;
	bool HasFilename() const;
	wxString GetFilename() const;
	void SetFilename(wxString filename);
	
	bool OpenFile();
	bool SaveFile();
	
private:
	wxString m_filename;
};

#endif // EDITORPANEL_H
