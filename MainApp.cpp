#include "MainApp.h"
#include "MainFrame.h"


MainApp::MainApp()
{
}

MainApp::~MainApp()
{
}

bool MainApp::OnInit()
{
	MainFrame* pFrame = new MainFrame(NULL);
	SetTopWindow(pFrame);
	return pFrame->Show();
}

IMPLEMENT_APP(MainApp);
