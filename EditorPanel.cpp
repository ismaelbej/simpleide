#include "EditorPanel.h"
#include <wx/file.h>


EditorPanel::EditorPanel(wxWindow* parent)
    :EditorPanelBase(parent)
{
}

EditorPanel::~EditorPanel()
{
}

bool EditorPanel::IsModified() const
{
	return m_editor->IsModified();
}

bool EditorPanel::HasFilename() const
{
	return !m_filename.empty();
}

wxString EditorPanel::GetFilename() const
{
	return m_filename;
}

void EditorPanel::SetFilename(wxString filename)
{
	m_filename = filename;
}

bool EditorPanel::OpenFile()
{
	if (!HasFilename())
		return true;

	return m_editor->LoadFile(m_filename);
}
	
bool EditorPanel::SaveFile()
{
	if (!HasFilename())
		return false;

	return m_editor->SaveFile(m_filename);
}
